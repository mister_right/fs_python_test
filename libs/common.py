import io
import os
import requests
import uuid

from colorthief import ColorThief
from dotenv import load_dotenv
from pathlib import Path
from PIL import Image
from redis import Redis

load_dotenv()
BASE_DIR = Path(__file__).resolve().parent.parent
PHOTOS_DIR = os.path.join(BASE_DIR, 'static', 'photos')
SERVER_URL = os.getenv('SERVER_URL', 'http://127.0.0.1:8000')

rc = Redis()

def get_rc_key(key):
    v = rc.get(key)
    if v is None:
        return None
    return v.decode()


def get_env(k, default=None):
    return os.getenv(k, default)


def get_photo_data(url):
    r = requests.get(url)
    if r.status_code != 200:
        return None, None
    
    photo_bytes = io.BytesIO(r.content)
    photo = Image.open(photo_bytes)
    width, height = photo.size
    dominant_color_rgb = ColorThief(photo_bytes).get_color(quality=1)
    dominant_color_hex = '#%02x%02x%02x' % dominant_color_rgb
    data = {
        'width': width,
        'height': height,
        'dominant_color': dominant_color_hex,
    }

    return photo, data
