"""
THIS SCRIPT WILL WORK ONLY if  
"""
import django
import json
import os
import requests
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'python_test.settings'
django.setup()
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "python_test.settings")

from photos_manager.tasks import fetch_photos

def main():
    print('read readme.md')
    if len(sys.argv) != 3:
        return 1
    source = sys.argv[1]
    p = sys.argv[2]

    data = None
    if source == 'api':
        r = requests.get(p)
        if r.status_code != 200:
            print('invalid url')
            return
        data = r.json()

    elif source == 'file':
        if os.path.isfile(p) is False:
            print('file does not exist')
            return 1
        with open(p, 'r') as f:
            data = json.load(f)

    if data is None:
        print('invalid arguments, read readme.md')
        return 1
    
    fetch_photos(data)


if __name__ == '__main__':
    main()
