import json
import psycopg2
import requests
import uuid

from celery import shared_task
from concurrent.futures import ThreadPoolExecutor
from libs.common import get_env, get_photo_data, PHOTOS_DIR
from photos_manager.models import Photo
from photos_manager.serializers import PhotoSerializer
from psycopg2.extras import execute_values
from time import sleep


def download_photo(p):
    photo, data = get_photo_data(p['url'])
    if photo is None:
        return None

    filename = str(uuid.uuid4()) + '.' + photo.format
    photo.save(f'{PHOTOS_DIR}/{filename}', photo.format)
    data['filename'] = filename
    data['title'] = p['title']
    data['album_id'] = p['albumId']
    return data


@shared_task()
def fetch_photos(photos: list) -> None:
    n_threads = 10
    pool = ThreadPoolExecutor(n_threads)
    future_threads = []

    d, dd = 0, 0
    for photo in photos:
        future = pool.submit(download_photo, photo)
        future_threads.append(future)
        dd += 1

    data = []
    for future in future_threads:
        result = future.result()
        if result is not None:
            data.append(result)
        if d % 10 == 0:
            print(f'{d} / {dd}')
        d += 1
    
    batch_size = 100
    for i in range(0, len(data), batch_size):
        print(f'insert and save {i} - {i + batch_size}')
        batch = data[i:i+100]
        ready_batch = []
        for j in batch:
            tmp = Photo(title=j['title'], album_id=j['album_id'], width=j['width'], height=j['height'], dominant_color=j['dominant_color'], filename=j['filename'])
            ready_batch.append(tmp)
        Photo.objects.bulk_create(ready_batch)

    print('end')