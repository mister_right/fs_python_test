from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from photos_manager.models import Photo


class PhotoTests(APITestCase):

    def test_photo_list(self):
        res = self.client.get('/api/photos/')
        self.assertEqual(res.status_code, status.HTTP_200_OK)
    
    def test_photo_create(self):
        data = {'album_id': 1, 'title': 'title', 'url': 'https://filipkowalewski.xyz/static/city.jpeg'}
        res = self.client.post('/api/photos', data=data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
