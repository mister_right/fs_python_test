from django.db import models


class Photo(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=1000, null=False)
    album_id = models.IntegerField(null=False)
    width = models.IntegerField(null=False)
    height = models.IntegerField(null=False)
    dominant_color = models.CharField(max_length=7, null=False)
    filename = models.CharField(max_length=255, null=False)
