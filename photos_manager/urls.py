from django.urls import path
from rest_framework.routers import DefaultRouter, SimpleRouter
from .views import PhotosViewSet, create_photos_from_external_api, create_photos_from_json_file

router = DefaultRouter()
router.register(r'photos', PhotosViewSet, basename='photo')
urlpatterns = router.urls
urlpatterns.append(path('photos/api-batch', create_photos_from_external_api))
urlpatterns.append(path('photos/json-batch', create_photos_from_json_file))