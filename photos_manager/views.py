import json
import os
import requests
import uuid

from django.http import HttpResponse
from libs.common import get_photo_data, get_rc_key, PHOTOS_DIR, rc 
from rest_framework import status, viewsets
from rest_framework.decorators import api_view, parser_classes
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

from .tasks import fetch_photos
from .models import Photo
from .serializers import PhotoSerializer


@api_view(['POST'])
def create_photos_from_external_api(request):
    api_url = request.data.get('api_url', None)
    if api_url is None:
        return Response({'error': 'api_url field required'}, status=status.HTTP_400_BAD_REQUEST)

    r = requests.get(api_url)
    if r.status_code != 200:
        return Response({'error': f'{url}: status code {r.status_code}'}, status=status.HTTP_400_BAD_REQUEST)

    res = fetch_photos.delay(r.json())
    return Response({'prcodess_id': str(res)})


@api_view(['POST'])
@parser_classes([MultiPartParser])
def create_photos_from_json_file(request):
    file = request.FILES['file'].file
    try:
        file_dict = json.load(file)
    except UnicodeDecodeError:
        return Response({'error': 'invalid file'}, status=status.HTTP_400_BAD_REQUEST)

    res = fetch_photos.delay(file_dict)
    return Response({'prcodess_id': str(res)})


class PhotosViewSet(viewsets.ViewSet):

    def list(self, request):
        photos = Photo.objects.all().order_by('id')
        serializer = PhotoSerializer(photos, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk):
        filename = get_rc_key(f'photos.{pk}.name')
        print(filename)
        if filename is None:
            p = Photo.objects.filter(id=pk).first()
            if p is None:
                return Response(status=status.HTTP_404_NOT_FOUND)
            filename = p.filename
            rc.set(f'photos.{pk}.name', filename)

        filepath = f'{PHOTOS_DIR}/{filename}'
        if os.path.isfile(filepath) is False:
            return Response(status=status.HTTP_404_NOT_FOUND)
        with open(f'{PHOTOS_DIR}/{filename}', 'rb') as f:
            return HttpResponse(f.read(), 'image/jpeg')

    def create(self, request):
        album_id = request.data.get('album_id', None)
        title = request.data.get('title', None)
        url = request.data.get('url',  None)
        if url is None:
            return Response({'error': 'url field required'}, status=status.HTTP_400_BAD_REQUEST)

        data = { 'title': title, 'album_id': album_id }
        photo, photo_data = get_photo_data(url)
        if photo is None:
            return Response({'error': f'photo download fail'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        data.update(photo_data)
        data['filename'] = str(uuid.uuid4()) + '.' + photo.format
        photo.save(os.path.join(PHOTOS_DIR, data['filename']), photo.format)

        serializer = PhotoSerializer(data=data)
        if serializer.is_valid() is False:
            return Response(serializer.errors)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def partial_update(self, request, pk):
        try:
            photo_obj = Photo.objects.get(id=pk)
        except Photo.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        album_id = request.data.get('album_id', None)
        title = request.data.get('title', None)
        url = request.data.get('url',  None)

        data = {}
        if album_id is not None:
            data['album_id'] = album_id
        if title is not None:
            data['title'] = title

        if url is not None:
            photo, photo_data = get_photo_data(url)
            data.update(photo_data)
            data['filename'] = str(uuid.uuid4()) + '.' + photo.format

        if not data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = PhotoSerializer(photo_obj, data=data, partial=True)
        if serializer.is_valid() is False:
            return Response(serializer.errors)

        if url is not None:
            photo.save(os.path.join(PHOTOS_DIR, data['filename']), photo.format)

        serializer.save()
        return Response(data)

    def destroy(self, request, pk):
        try:
            photo = Photo.objects.get(id=pk)
        except Photo.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        photo.delete()
        return Response({'message': f'photo {pk} deleted.'})
