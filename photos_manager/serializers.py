from libs.common import SERVER_URL
from rest_framework import serializers
from .models import Photo


class PhotoSerializer(serializers.ModelSerializer):

    url = serializers.SerializerMethodField('get_photo_url')

    class Meta:
        model = Photo
        fields = ('id', 'title', 'album_id', 'width', 'height', 'dominant_color' ,'url', 'filename')
        extra_kwargs = {'filename': {'write_only': True}}

    def get_photo_url(self, photo):
        return f'{SERVER_URL}/api/photos/{photo.id}'
