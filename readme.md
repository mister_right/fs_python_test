# Photo python test

This is simple backend application for managing photos. Python 3.10

# Usage
In order to run application postgresql and redis must be installed.

```
git pull https://gitlab.com/mister_right/fs_python_test
cd fs_python_test
pip install -r requirements.txt
complete .env file
run django migrations
<!-- start celery worker in another window -->
celery -A python_test worker -l INFO
python manage.py runserver
```

# REST API

## Get list of photos

`GET /api/photos/`

```
curl --request GET \
  --url http://127.0.0.1:8000/api/photos
```

## Display photo

`GET /api/photos/:pk`

```
curl --request POST \
  --url http://127.0.0.1:8000/api/photos/ \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "some title",
	"album_id": "2",
	"url": "https://picsum.photos/200"
}'
```

## Add new photo

`POST /api/photos`

```
curl --request POST \
  --url http://127.0.0.1:8000/api/photos/ \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "title title",
	"album_id": "2",
	"url": "https://picsum.photos/200"
}'
```

## Update photo

`PATCH /api/photos/:pk`

```
curl --request PATCH \
  --url http://127.0.0.1:8000/api/photos/735/ \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "new title",
	"album_id": 32123,
	"url": "https://picsum.photos/200"
}'
```

## Delete photo

`DELETE /api/photos/:pk`

```
curl --request DELETE \
  --url http://127.0.0.1:8000/api/photos/800
```

## Import photos from external api

`POST /api/photos/api-batch`

```
curl --request POST \
  --url http://127.0.0.1:8000/api/photos/api-batch \
  --header 'Content-Type: application/json' \
  --data '{
	"api_url": "https://pierdolnik.online/static/dd.json"
}'
```

## Import photos from local file

`POST /api/photos/json-batch`

```
curl --request POST \
  --url http://127.0.0.1:8000/api/photos/json-batch \
  --header 'content-type: multipart/form-data' \
  --header 'filename: filename' \
  --form file=@filepath
```

# CLI Usage

```
# to import photos from external api run
python photos_manager_cli.py url 'api_url_here'

# to import photos from file local file run
python photos_manager_cli.py file 'path_to_local_json_file'
```
